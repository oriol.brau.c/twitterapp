package com.acciona.codechallenge.service;

import com.acciona.codechallenge.models.Tweet;
import com.acciona.codechallenge.models.User;
import com.acciona.codechallenge.repositories.TweetRepository;
import com.acciona.codechallenge.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class APIService {

    @Autowired
    private TweetRepository tweetRepository;

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    public List<Tweet> getTweets(){
        return tweetRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Tweet getTweet(Long id){
        return tweetRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public User getUser(Long id){
        return userRepository.findById(id);
    }

    @Transactional
    public Tweet setTweetStatus(Long id, int value) throws Exception {
        Tweet tweet;
        if(id!= null){
            tweet = tweetRepository.findById(id);
            tweet.setValidated(value);
            tweetRepository.save(tweet);
        }else{
            throw new Exception();
        }
        return tweet;
    }

    @Transactional(readOnly = true)
    public List<Tweet> getTweetsFromsUser(Long id) {
        return tweetRepository.getValidatedTweetByUser(id);
    }
}
