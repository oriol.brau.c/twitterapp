package com.acciona.codechallenge.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name="users")
public class User {

    @Id
    private Long id;

    @Column
    private String username;

    public User(){

    }

    public User(Long id, String username){
        this.id = id;
        this.username = username;
    }

}
