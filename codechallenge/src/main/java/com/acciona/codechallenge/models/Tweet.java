package com.acciona.codechallenge.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name="tweets")
public class Tweet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 500)
    private String text;

    @ManyToOne
    private User user;

    @Column
    private String location;

    @Column
    private int validated;
}
