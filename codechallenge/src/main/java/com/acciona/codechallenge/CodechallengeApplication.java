package com.acciona.codechallenge;

import com.acciona.codechallenge.core.twitter4j.StreamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableSwagger2
@EnableJpaRepositories
public class CodechallengeApplication {

    @Autowired
    private StreamService streamService;

    public static void main(String[] args) {
        SpringApplication.run(CodechallengeApplication.class, args);
    }

    @PostConstruct
    public void init(){
        String [] languages = {"es","fr","it"};
        String [] hashtag = {"Abascal","Mobile"};
        int nFollowers = 100;
        streamService.getStream(hashtag,nFollowers,languages);
    }
}
