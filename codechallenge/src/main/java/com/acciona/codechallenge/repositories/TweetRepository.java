package com.acciona.codechallenge.repositories;

import com.acciona.codechallenge.models.Tweet;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TweetRepository extends CrudRepository<Tweet, String> {

    @Query("select tweet from tweets tweet order by tweet.id asc")
    List<Tweet> findAll();

    @Query("select tweet from tweets tweet where tweet.id = :id")
    Tweet findById(@Param("id") Long id);

    @Query("select tweet from tweets tweet where tweet.id = :id")
    void deleteById(@Param("id") Long id);

    @Query("select tweet from tweets tweet where tweet.user.id = :id and tweet.validated = 1")
    List<Tweet> getValidatedTweetByUser(@Param("id") Long id);

}
