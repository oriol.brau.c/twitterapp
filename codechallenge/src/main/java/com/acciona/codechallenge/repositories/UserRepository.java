package com.acciona.codechallenge.repositories;

import com.acciona.codechallenge.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<User, String> {

    @Query("select user from users user where user.id = :id")
    User findById(@Param("id") Long id);

}