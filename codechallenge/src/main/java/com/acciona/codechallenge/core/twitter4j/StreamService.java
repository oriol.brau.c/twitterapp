package com.acciona.codechallenge.core.twitter4j;

import com.acciona.codechallenge.models.Tweet;
import com.acciona.codechallenge.models.User;
import com.acciona.codechallenge.repositories.TweetRepository;
import com.acciona.codechallenge.repositories.UserRepository;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import twitter4j.*;

@Service
@Slf4j
public class StreamService {

    @Autowired
    private Twitter twitter;

    @Autowired
    private TweetRepository tweetRepository;

    @Autowired
    private UserRepository userRepository;

    private TwitterStream twitterStream;

    public TwitterStream getStream(String[] hashtag, int nFollowers, String[] languages){

        this.twitterStream = new TwitterStreamFactory(twitter.getConfiguration()).getInstance();

        StatusListener listener = new StatusListener() {

            @Override
            public void onStatus(Status status) {
                if(status.getUser().getFollowersCount() > nFollowers){

                    User us1 = new User(status.getUser().getId(), status.getUser().getName());

                    userRepository.save(us1);

                    Tweet t = new Tweet();
                    t.setText(status.getText());
                    t.setUser(us1);
                    t.setLocation(status.getUser().getLocation());
                    t.setValidated(0);

                    System.out.println("Saved: |"+status.getUser().getLocation()+"| "+" @" + status.getUser().getName() + " ---- " + status.getText());
                    tweetRepository.save(t);
                }
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                log.warn("onDeletionNotice: {}",statusDeletionNotice);
            }

            @Override
            public void onTrackLimitationNotice(int i) {
                log.warn("onTrackLimitationNotice: {}",i);
            }

            @Override
            public void onScrubGeo(long l, long l1) {
                log.warn("onScrubGeo: {}",l);
            }

            @Override
            public void onStallWarning(StallWarning stallWarning) {
                log.warn("onStallWarning: {}",stallWarning);
            }

            @Override
            public void onException(Exception e) {
                log.warn("Exception: ",e);
            }
        };

        FilterQuery fq = new FilterQuery();
        if(hashtag!=null){
            fq.track(hashtag);
        }
        if(languages!=null){
            fq.language(languages);
        }
        this.twitterStream.addListener(listener);
        this.twitterStream.filter(fq);
        return this.twitterStream;
    }

    public TwitterStream getTwitterStream() {
        return this.twitterStream;
    }

}
