package com.acciona.codechallenge.controller;

import com.acciona.codechallenge.core.twitter4j.StreamService;
import com.acciona.codechallenge.core.twitter4j.StreamStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import twitter4j.*;

@Slf4j
@RestController
@RequestMapping("/api")
public class TwitterController {

    @Autowired
    private StreamService streamService;

    @Autowired
    private Twitter twitter;

    private TwitterStream twitterStream;

    @GetMapping("/stream/{status}")
    public void getStream(@PathVariable("status") StreamStatus status,  @RequestParam(defaultValue = "europa") String [] hashtags, @RequestParam(defaultValue = "1500") int nFollowers,  String [] languages){
        initStream(status, hashtags, nFollowers, languages);
    }

    @GetMapping("/locations")
    public ResponseList<Location> getLocation() throws TwitterException {
        return twitter.getAvailableTrends();
    }

    //Obtener N hashtags más usados
    @GetMapping("/hashtags/toprated")
    public Trend [] getTrends(@RequestParam(value = "n",defaultValue = "10") int n) throws TwitterException {
        //SpainCode = 753692;
        Trends t = twitter.trends().getPlaceTrends(753692);
        Trend[] toprated = t.getTrends();
        Trend [] response = new Trend[n];
        for (int i = 0; i < toprated.length && i < n; i++) {
            response[i] = toprated[i];
        }

        return response;
    }

    public void initStream(StreamStatus status, String[] hashtags, int nFollowers ,String [] languages){
        if(status.equals(StreamStatus.on)){
            twitterStream = streamService.getStream(hashtags, nFollowers, languages);
        }else if(status.equals(StreamStatus.off)){
            twitterStream = streamService.getTwitterStream();
            shutDownStream(twitterStream);
        }
    }

    public static void shutDownStream(TwitterStream twitterStream){
        if(twitterStream != null){
            twitterStream.cleanUp();
            twitterStream.clearListeners();
            twitterStream.shutdown();
        }
    }

}
