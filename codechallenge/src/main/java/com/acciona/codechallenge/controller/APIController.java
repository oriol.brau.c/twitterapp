package com.acciona.codechallenge.controller;

import com.acciona.codechallenge.models.Tweet;
import com.acciona.codechallenge.models.User;
import com.acciona.codechallenge.service.APIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class APIController {

    @Autowired
    private APIService apiService;

    public APIController(APIService apiservice){
        this.apiService = apiservice;
    }

    //Obtener todos los tweets
    @GetMapping(value = "/tweets", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Tweet> getStoredTweets(){
        return apiService.getTweets();
    }

    //Obtener un tweet
    @GetMapping(value = "/tweets/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Tweet getTweet(@PathVariable("id") Long id){
        return apiService.getTweet(id);
    }

    //Validar un tweet
    @PatchMapping(value = "/tweets/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Tweet updateTweetStatus(@PathVariable("id") Long id, @RequestParam(value="status",defaultValue = "1") int value) throws Exception {
        return apiService.setTweetStatus(id, value);
    }

    //Obtener los tweets validados por un usuario
    @GetMapping(value = "/tweets/validated/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Tweet> getValidatedTweetsByUser(@PathVariable("id") Long id){
        return apiService.getTweetsFromsUser(id);
    }

}
